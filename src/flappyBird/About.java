package flappyBird;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class About extends JFrame{
    private Container c;
    private JButton b1;
    private ImageIcon img;
    private Cursor cursor;

    
    About(){
    	Ab();
   }
    
    public void Ab() {
         c = this.getContentPane();
         c.setLayout(null);
         c.setBackground(Color.CYAN);
         
         Font f = new Font("Algerian", Font.BOLD, 20);
         cursor = new Cursor(Cursor.HAND_CURSOR);
         
         
         JLabel ll = new JLabel();
         ll.setText(" Desktop Application");
         ll.setBounds(195,10,350,50);
         ll.setFont(f);
         c.add(ll);
         
          JLabel l2 = new JLabel();
         l2.setText(" 2019");
         l2.setBounds(245,30,350,70);
         l2.setFont(f);
         c.add(l2);
         
         
         JLabel l3= new JLabel();
         l3.setText(" Building and Developing By :");
         l3.setBounds(55,80,350,70);
         l3.setFont(f);
         c.add(l3);
         
         Font f2 = new Font("Algerian", Font.BOLD, 20);
         
         JLabel l4= new JLabel();
         l4.setText(" Moonmoon Das");
         l4.setBounds(155,130,350,70);
         l4.setFont(f2);
         c.add(l4);
         
         JLabel l5= new JLabel();
         l5.setText(" Akash Chandra Deb Nath");
         l5.setBounds(155,160,350,70);
         l5.setFont(f2);
         c.add(l5);
         
         JLabel l6= new JLabel();
         l6.setText(" Al Adnan Sami");
         l6.setBounds(155,190,350,70);
         l6.setFont(f2);
         c.add(l6);
         

 	     img = new ImageIcon("Images/Backarrow.png");
         b1 = new JButton(img);
         b1.setBounds(70,350,80,50);
         b1.setCursor(cursor);
         c.add(b1);
         
         b1.addActionListener(new ActionListener() {

 			@Override
 			public void actionPerformed(ActionEvent e) {
 				//MainBird m = new MainBird();
 				 MenuPage frame = new MenuPage();
 				dispose();
 				
 				
 			}
 	    	
 	    });
 		
         
         setTitle("About");
		 setSize(600, 800);
         setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 // frame.setResizable(true);
         setVisible(true);
		 setLocationRelativeTo(null);
      
    }
    
     public static void main(String[] args) {
        
         About frame = new About();                       
		  
		 
    }  

}
