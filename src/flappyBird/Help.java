package flappyBird;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class Help extends JFrame {
    
    public Container c;
    private JButton b1;
    private ImageIcon img;
    private Cursor cursor;

    
    Help()
    {       
        initComponents();   
    
    }
    public void initComponents(){
         c = this.getContentPane();
         c.setLayout(null);
         c.setBackground(Color.CYAN);
         //c.setResizeable(false);
        
        Font f = new Font("Algerian", Font.BOLD, 24);
        cursor = new Cursor(Cursor.HAND_CURSOR);
        
        
       /* ImageIcon i = new ImageIcon(getClass().getResource("flappp.png"));
       JLabel li = new JLabel(i);
    li.setBounds(0,0,600,800);
    c.add(li);*/
        
        
         JLabel ll = new JLabel();
         ll.setText(" The procedure to play the Game");
         ll.setBounds(80,10,550,50);
         ll.setFont(f);
        // ll.setBackground(Color.BLUE)
         c.add(ll);
         
         Font f2 = new Font("Algerian", Font.BOLD, 18);
        
         JLabel l = new JLabel();
         l.setText(" 1.Press mouse to keep bird flying.");
         l.setBounds(45,80,550,50);
         l.setFont(f2);
         c.add(l);
         
         
          JLabel l2 = new JLabel();
         l2.setText(" 2.If you not press mouse the bird will fall down.");
         l2.setBounds(45,120,550,50);
         l2.setFont(f2);
         c.add(l2);
         
          JLabel l3= new JLabel();
         l3.setText(" 3.You have to going on the space of pipes.");
         l3.setBounds(45,160,550,50);
         l3.setFont(f2);
         c.add(l3);
         
         img = new ImageIcon("Images/Backarrow.png");
         b1 = new JButton(img);
         b1.setBounds(70,350,80,50);
         b1.setCursor(cursor);
         c.add(b1);
         
         b1.addActionListener(new ActionListener() {

  			@Override
  			public void actionPerformed(ActionEvent e) {
  				//MainBird m = new MainBird();
  				 MenuPage frame = new MenuPage();
  				dispose();
  				
  				
  			}
  	    	
  	    });
         
         setTitle("Help");
		 setSize(600, 800);
         setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 // frame.setResizable(true);
         setVisible(true);
		 setLocationRelativeTo(null);
    
    }

    
    
    
    public static void main(String[] args) {
        
        
         
                  Help frame = new Help();         
                 /* frame.setTitle("Help");
         		 frame.setSize(600, 800);
                  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         		 // frame.setResizable(true);
                  frame.setVisible(true);
         		frame.setLocationRelativeTo(null);*/
		  
		  //frame.pack();
		  
		  
    }
    
}