package flappyBird;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HomePage extends JFrame {

    private Container c;
    private JButton b1, b2, exit;
    private Font f;
    private Cursor cursor;
    public FileWriter fr;
    private ImageIcon img, img2, img3, img4;

    public HomePage() {
        initComponents();
    }

    private void initComponents()  {
        
        
       
        c = this.getContentPane();
        c.setLayout(null);
        //c.setBackground(Color.CYAN);

        f = new Font("Arial", Font.BOLD, 26);
        cursor = new Cursor(Cursor.HAND_CURSOR);

        ImageIcon img = new ImageIcon("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\homebird.png");
        JLabel l = new JLabel(img);
        c.add(l);
        l.setBounds(200, 60, 200, 200);
        c.add(l);

        ImageIcon img4 = new ImageIcon("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\Flappy_Logo.png");
        JLabel la = new JLabel(img4);
        c.add(la);
        la.setBounds(100, 300, 400, 150);
        c.add(la);

        img2 = new ImageIcon("Images/Backarrow2.jpg");
        b1 = new JButton(img2);
        c.add(b1);
        b1.setBounds(120, 500, 80, 80);
        b1.setCursor(cursor);
        c.add(b1);

        img3 = new ImageIcon("Images/Forwardarrow.jpg");
        b2 = new JButton(img3);
        c.add(b2);
        b2.setBounds(400, 500, 80, 80);
        b2.setCursor(cursor);
        c.add(b2);

        ImageIcon back = new ImageIcon("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\baacc.jpg");
        JLabel l1 = new JLabel(back);
        c.add(l1);
        l1.setBounds(0, 0, 600, 800);
        c.add(l1);

        exit = new JButton("Exit");
        c.add(exit);
        exit.setBounds(250, 650, 110, 60);
        exit.setFont(f);
        exit.setCursor(cursor);
        exit.setBackground(Color.GREEN);
        exit.setForeground(Color.RED);

        setVisible(true);
        setSize(600, 800);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Home Page");

        b1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                MenuPage frame = new MenuPage();
                dispose();

            }

        });

        b2.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //MenuPage frame = new MenuPage();
            	try {
            		MainBird mb = new MainBird();
                    dispose();
            	}
            	catch(Exception ea) {
            		ea.printStackTrace();
            	}
                

            }

        });

        exit.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                //HomePage hp = new HomePage();
                // hp.setVisible(false);
                // hp.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                System.exit(0);

            }

        });

        /* Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				 new GameMusic1().music();
				
			}
			
		});*/
    }

    public static void main(String[] args) {
        HomePage frame = new HomePage();

    }

}
