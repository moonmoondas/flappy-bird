package flappyBird;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class GamePanel extends JPanel {

    private static final long serialVersionUID = 1L;
    public static final int WIDTH = 600;
    public static final int HEIGHT = 800;
    //int width_wall = 60;
    //int height = HEIGHT-Y;

    public static boolean Gameover = false;
    public static int score = 0;
    public static boolean starting = false;
    //public static int proceed = -1;
    public static boolean Gameover2 = false;

    public static Timer timer, timer2;
    private int proceed = 5;

    private int xCoor = 0;
    private BufferedImage img, img2;

    
      
    BirdImage bi;
   

    WallImage wi = new WallImage(GamePanel.WIDTH);
    WallImage wi2 = new WallImage(GamePanel.WIDTH + (GamePanel.WIDTH / 2));
    //private int height;

    public GamePanel() throws IOException {
         bi = new BirdImage();
        LoadImage();

        timer = new Timer(30, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //gp.repaint();
                //gp.Move();
                repaint();
                Move();
            }
        });
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                bi.goUpwards();
            }
        });

        timer2 = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                proceed--;
                //GamePanel.proceed=proceed;
                starting = true;
                repaint();
                if (proceed == 0) {
                    timer2.stop();
                    timer.start();
                    starting = false;
                }

            }

        });
        timer2.start();
    }

    private void LoadImage() {
        try {
            img = ImageIO.read(new File("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\Background.jpg"));
            img2 = ImageIO.read(new File("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\Background.jpg"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(img, xCoor, 0, null);
        g.drawImage(img2, xCoor + 1550, 0, null);
        bi.drawBird(g);
        wi.drawWall(g);
        wi2.drawWall(g);

        g.setFont(new Font("Tahoma", Font.BOLD, 40));

        g.drawString("Score " + score, WIDTH / 2, 100);

        if (starting) {
            g.setFont(new Font("Tahoma", Font.BOLD, 150));
            g.drawString(Integer.toString(proceed), WIDTH / 2 - 75, 250);
        }

        /*if(Gameover2) {
			g.setFont(new Font("Tahoma",Font.BOLD,100));
			//g.drawString(Integer.toString(proceed), WIDTH/2-75, 250);
			g.drawString("Gameover"+"\nYour score is : "+score, WIDTH/2, 100);
		}*/
 /*if(Gameover) {
			g.setFont(new Font("Tahoma",Font.BOLD,150));
			//g.drawString(Integer.toString(proceed), WIDTH/2-75, 250);
			g.drawString("Your score is : "+score, WIDTH/2, 100);
		}*/
    }

    public void Move() {
        bi.birdMovement();
        wi.wallMovement();
        wi2.wallMovement();

        if (Gameover) {
            wi.X = GamePanel.WIDTH;
            wi2.X = GamePanel.WIDTH + (GamePanel.WIDTH / 2);
            Gameover = false;
        }

        xCoor += -6;

        if (xCoor == -2400) {
            xCoor = 0;
        }

        // System.out.println(wi.X+"->"+BirdImage.x +"    :    "+ wi2.X+"->"+BirdImage.x);
        if (wi.X == BirdImage.x || wi2.X == BirdImage.x) {
            score += 1;
            System.out.println(score);
        }
    }

    public static boolean popUpMessage() {
        int result = JOptionPane.showConfirmDialog(null, "Game Over, Your score is : " + score + "\nDo you want to restart the game", "Game Over", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            return true;
        } else {
            return false;
        }
    }

    /*timer = new Timer(30,new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			this.paint();
			gp.Move();
		}
	});*/
}
