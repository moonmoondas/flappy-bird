package flappyBird;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class BirdImage {

    private BufferedImage img = null;
    private static int bird_dia = 36; //bird accurate width
    public static int x = (GamePanel.WIDTH / 2) - (bird_dia / 2);
    public static int y = (GamePanel.HEIGHT) / 2;
    FileReader fr;
    private static int speed = 2;
    private int accelaration = 1;
    int p;
    Character c = new Character('y');

    public BirdImage() throws FileNotFoundException, IOException {

        fr = new FileReader("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\choice1.txt");
        p = fr.read();
        fr.close();
        c = (char) p;
        LoadImage();
    }

    private void LoadImage() {
        try {
            if(c.equals('1'))
            {
                  img = ImageIO.read(new File("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\1.png"));
            }
            if(c.equals('2'))
            {
                  img = ImageIO.read(new File("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\Bird (2).png"));
            }
              if(c.equals('3'))
            {
                  img = ImageIO.read(new File("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\Bird3.png"));
            }
                if(c.equals('4'))
            {
                  img = ImageIO.read(new File("E:\\Falguni Mam\\3rd semester\\Project\\Flappy Bird\\Flappybirf\\Images\\Bird 4.png"));
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void drawBird(Graphics g) {
        g.drawImage(img, x, y, null);
    }

    public void birdMovement() {
        if (y >= 0 && y <= GamePanel.HEIGHT) {
            speed += accelaration;//3,4,5
            y += speed;//400+3,400+3+4,400+3+4+5
        } else {
            boolean option = GamePanel.popUpMessage();
            if (option) {
                try {
                    Thread.sleep(500);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                reset();
            } else {
                //close window
                //JFrame frame = MainBird.getFrame();
                //new MainBird();
                //frame.dispose();
                //dispose();
                HomePage frame = new HomePage();
                GamePanel.timer.stop();

            }
            //reset();
        }
    }

    public void goUpwards() {
        speed = -17;
    }

    public static void reset() {
        speed = 2;
        y = GamePanel.HEIGHT / 2;
        GamePanel.score = 0;
    }

    public static Rectangle getBirdRect() {
        Rectangle birdRect = new Rectangle(x, y, bird_dia, 35);
        return birdRect;
    }

}
